# Match Day Data Service

Accept post requests with a payload and link to external xml feed of football match stats. Persist records and return appropriate JSON response. 

## Tech stack

* Swagger openAPI 3.0 endpoint documented before starting, please view docs for yaml or json version.
* TDD - Went with PHPUnit for unit and integration testing. Behat for E2E functional tests. 
* Framework - Lumen for light weight frame work for a public exposed API.
* DDD - Doctrine 2 instead of active records eloquent to aid in striving for domain driven design.
* Additional - Fractal for transformers for repsonses. Form request package to utilise Http Validation layer that is in Laravel however missing from Lumen. GuzzleHttp planned for acquiring XML file hosted else where.

### Explain reasoning

Throughout codebase tried to explain my thought process where not obvious. The way I modelled the data is in an attempt to future proof the application. Instead of a one to many association from the match to the teams, I gave the 
team the owning side and allowed the match to store a home team and an away team. The stats that record the individual player records bring in the player to the match itself with the side they played on, instead of tieing the player to the team. 
Did this because as player could possibly have more than one team (domestic, international, loanee) or transfer to another club. So by tieing the match to the players who played it the integrity of the match statistics
will remain intact in the future if players where to move. 

### Tests

Run PHPUnit (vendor/bin/phpunit) and Behat (vendor/bin/behat with tests run in memory).