<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * Cater for any possible sport with common attributes.
 * @ORM\Entity
 * @ORM\Table(name="matches")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="sport", type="string")
 */
abstract class AbstractMatch {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=60)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $competition;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function setCompetition(string $competition) {
        $this->competition = $competition;
    }

    public function getCompetition(): string {
        return $this->competition;
    }

    public function setMatchId(int $matchId) {
        $this->id = $matchId;
    }

    public function getMatchId(): int {
        return $this->id;
    }

    public function setCreatedAt(string $createdAt) {
        $this->createdAt = new DateTime($createdAt);
    }

    public function getCreatedAt(): DateTime {
        return $this->createdAt;
    }

    public function setUpdatedAt(string $updatedAt) {
        $this->updatedAt = new DateTime($updatedAt);
    }

    public function getUpdatedAt(): DateTime {
        return $this->updatedAt;
    }

}
