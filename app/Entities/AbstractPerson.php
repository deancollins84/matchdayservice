<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Idea here is to extend this for match officials, managers, support staff, etc ...
 * @ORM\Entity
 * @ORM\Table(name="people")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 */
abstract class AbstractPerson implements Interfaces\PersonInterface {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=60)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $lastName;

    public function setId(string $id) {
        $this->id = $id;
    }

    public function getId(): string {
        return $this->id;
    }

    public function setFirstName(string $firstName) {
        $this->firstName = $firstName;
    }

    public function getFirstName(): string {
        return $this->firstName;
    }

    public function setLastName(string $lastName) {
        $this->lastName = $lastName;
    }

    public function getLastName(): string {
        return $this->lastName;
    }

}
