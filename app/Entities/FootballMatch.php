<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity()
 */
class FootballMatch extends AbstractMatch implements Interfaces\MatchInterface, Interfaces\FootballMatchInterface {

    /**
     * @ORM\Column(type="integer")
     */
    private $season;
    
    /** Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="homeMatches", cascade={"persist"})
     * @ORM\JoinColumn(name="home_team_id", referencedColumnName="id")
     */
    private $homeTeam;
    
    /** Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="awayMatches", cascade={"persist"})
     * @ORM\JoinColumn(name="away_team_id", referencedColumnName="id")
     */
    private $awayTeam;

    /**
     * Went with the route of tieing matches to players stats for historical reasons, 
     * if players change team down the line they can still be referenced
     * as being part of said match. 
     * One matches can have many player stats.
     * @ORM\OneToMany(targetEntity="FootballMatchPlayerStat", mappedBy="footballMatch", cascade={"persist"}) 
     */
    protected $footballMatchPlayerStats;

    public function __construct() {
        $this->footballMatchPlayerStats = new ArrayCollection();
    }

    public function setId(string $id) {
        $this->id = $id;
    }

    public function getId(): string {
        return $this->id;
    }

    public function setSeason(int $season) {
        $this->season = $season;
    }

    public function getSeason(): int {
        return $this->season;
    }

    public function setHomeTeam($homeTeam){
        $this->homeTeam = $homeTeam;
    }
    
    public function getHomeTeam() : Team{
        return $this->homeTeam;
    }
    
    public function setAwayTeam($awayTeam){
        $this->awayTeam = $awayTeam;
    }
    
    public function getAwayTeam() : Team {
        return $this->awayTeam;
    }
    
    public function addFootballPlayerStat(FootballMatchPlayerStat $footballMatchPlayerStat) {
        $this->footballMatchPlayerStats->add($footballMatchPlayerStat);
    }
    
    public function setFootballPlayerStats(array $footballMatchPlayerStats){
        $this->footballMatchPlayerStats = new ArrayCollection($footballMatchPlayerStats);
    }
    
    public function getFootballPlayerStats() {
        return $this->footballMatchPlayerStats;
    } 
    
    /**
     * Full time winner based on goals to cater for a draw.
     * Would need further thought if service should winner after extra time and penalties.
     */
    public function getWinner(): ? Team{
        $home = $this->sumTeamStat('getGoals');
        $away = $this->sumTeamStat('getGoals', false);
        if($home == $away){
            return null; // Draw
        } else {
            return $home > $away ? $this->getHomeTeam() : $this->getAwayTeam();
        }
    }
    
    public function getTopScorer(): ? TeamPlayer {
        $maxGoals = 0;
        $maxGoalStat = null;
        foreach ($this->footballMatchPlayerStats as $stat) {
            $currentGoals = $stat->getGoals();
            if($currentGoals > $maxGoals){
                $maxGoals = $currentGoals;
                $maxGoalStat = $stat;
            }
        }
  
        
       return $maxGoalStat ? $maxGoalStat->getTeamPlayer() : null;
    }

    public function getTotalGoals(): ? int {
        return $this->sumStat('getGoals');
    }

    public function getTotalRedCards(): ? int {
        return $this->sumStat('getRedCards');
    }

    /**
     * Logic would need to be thought about having a max of two yellow cards and then one red.
     * @return int
     */
    public function getTotalYellowCards(): ? int {
        return $this->sumStat('getYellowCards');
    }

    public function getHomeTeamTotalTackles(): ? int {
        return $this->sumTeamStat('getTackles');
    }

    public function getHomeTeamTotalTouches(): ? int {
        return $this->sumTeamStat('getTouches');
    }

    public function getHomeTeamTotalFouls(): ? int {
        return $this->sumTeamStat('getFouls');
    }

    public function getAwayTeamTotalTackles(): ? int {
        return $this->sumTeamStat('getTackles', false);
    }

    public function getAwayTeamTotalTouches(): ? int {
        return $this->sumTeamStat('getTouches', false);
    }

    public function getAwayTeamTotalFouls(): ? int {
        return $this->sumTeamStat('getFouls', false);
    }

    /**
     * Common sum on array collection.
     * @param string $method
     * @param int $count
     * @return int
     */
    protected function sumStat(string $method, int $count = 0): int {
        foreach ($this->footballMatchPlayerStats as $stat) {
            if (method_exists($stat, $method)) {
                $count += $stat->$method();
            } else {
                $count = 0;
            }
        }
        return $count;
    }

    /**
     * Sum on given side.
     * @param string $method
     * @param bool $home
     * @param int $count
     * @return int
     */
    protected function sumTeamStat(string $method, bool $home = true, int $count = 0): int {
        foreach ($this->footballMatchPlayerStats as $stat) {
            if (method_exists($stat, $method)) {
                if ($home == $stat->isHome()) {
                    $count += $stat->$method();
                }
            } else {
                $count = 0;
            }
        }
        return $count;
    }

    public function __toString(): string {
        return 'football';
    }

}
