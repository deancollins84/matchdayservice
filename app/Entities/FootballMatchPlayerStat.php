<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Tied directly to the match.
 * Thinking here is that if went through match -> team -> players then these records
 * when they possibly change i.e. transfer the match as an aggregate route would
 * be effected.
 * @ORM\Entity
 * @ORM\Table(name="football_match_player_stats")
 */
class FootballMatchPlayerStat implements Interfaces\FootballMatchPlayerStatInterface {
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $side;

    /**
     * @ORM\Column(type="integer")
     */
    protected $goals;

    /**
     * @ORM\Column(type="integer")
     */
    protected $yellowCards;
    
   /**
     * @ORM\Column(type="integer")
     */
    protected $redCards;
    
     /**
     * @ORM\Column(type="integer")
     */
    protected $tackles;
    
     /**
     * @ORM\Column(type="integer")
     */
    protected $touches;
    
     /**
     * @ORM\Column(type="integer")
     */
    protected $fouls;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="FootballMatch", inversedBy="footballMatchPlayerStats", cascade={"persist"}) 
     * @ORM\JoinColumn(name="football_match_id", referencedColumnName="id", nullable=false) 
     */
    protected $footballMatch;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="TeamPlayer", inversedBy="footballMatchPlayerStats", cascade={"persist"}) 
     * @ORM\JoinColumn(name="team_player_id", referencedColumnName="id", nullable=false) 
     */
    protected $teamPlayer;
  
    public function setFootballMatch(FootballMatch $footballMatch) {
        $this->footballMatch = $footballMatch;
    }

    public function setTeamPlayer(TeamPlayer $teamPlayer) {
        $this->teamPlayer = $teamPlayer;
    }
    
    public function getTeamPlayer() : TeamPlayer {
        return $this->teamPlayer;
    }
   
    public function setSide(bool $side) {
        $this->side = $side;
    }

    public function getHomeOrAway(bool $home) {
        return $this->side ? 'home' : 'away';
    }
    
    public function isHome() : bool {
        return ($this->side) ? true : false;
    }

    public function setGoals(int $goals) {
        $this->goals = $goals;
    }

    public function getGoals(): ? int {
        return $this->goals;
    }

    public function setYellowCards(int $yellowCards) {
        $this->yellowCards = $yellowCards;
    }

    public function getYellowCards(): ? int {
        return $this->yellowCards;
    }

    public function setRedCards(int $redCards) {
        $this->redCards = $redCards;
    }

    public function getRedCards(): ? int {
        return $this->redCards;
    }
    
    public function setTackles(int $tackles) {
        $this->tackles = $tackles;
    }

    public function getTackles(): ? int {
        return $this->tackles;
    }
    
    public function setTouches(int $touches) {
        $this->touches = $touches;
    }

    public function getTouches(): ? int {
        return $this->touches;
    }
    
    public function setFouls(int $fouls) {
        $this->fouls = $fouls;
    }

    public function getFouls(): ? int {
        return $this->fouls;
    }

}
