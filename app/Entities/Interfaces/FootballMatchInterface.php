<?php

namespace App\Entities\Interfaces;

use App\Entities\Team;
use App\Entities\TeamPlayer;
use App\Entities\FootballMatchPlayerStat;
use Doctrine\Common\Collections\Collection;

interface FootballMatchInterface {
    public function setSeason(int $season);

    public function getSeason() : int;

    public function setHomeTeam($homeTeam);

    public function getHomeTeam() : Team;

    public function setAwayTeam($awayTeam);

    public function getAwayTeam() : Team;

    public function addFootballPlayerStat(FootballMatchPlayerStat $footballMatchPlayerStat);

    public function setFootballPlayerStats(array $footballMatchPlayerStats);

    public function getFootballPlayerStats();

    public function getWinner(): ? Team;

    public function getTopScorer(): ? TeamPlayer;

    public function getTotalGoals(): ? int;

    public function getTotalRedCards(): ? int;

    public function getTotalYellowCards(): ? int;

    public function getHomeTeamTotalTackles(): ? int;

    public function getHomeTeamTotalTouches(): ? int;

    public function getHomeTeamTotalFouls(): ? int;

    public function getAwayTeamTotalTackles(): ? int;

    public function getAwayTeamTotalTouches(): ? int;

    public function getAwayTeamTotalFouls(): ? int;
}
