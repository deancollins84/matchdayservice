<?php

namespace App\Entities\Interfaces;

use App\Entities\FootballMatch;
use App\Entities\TeamPlayer;

interface FootballMatchPlayerStatInterface {

    public function setFootballMatch(FootballMatch $footballMatch);

    public function setTeamPlayer(TeamPlayer $teamPlayer);
    
    public function getTeamPlayer() : TeamPlayer;
    
    public function setSide(bool $side);

    public function getHomeOrAway(bool $side);
    
    public function isHome() : bool;

    public function setGoals(int $goals);

    public function getGoals(): ? int;

    public function setYellowCards(int $yellowCards);

    public function getYellowCards(): ? int;

    public function setRedCards(int $redCards);

    public function getRedCards(): ? int;

    public function setTackles(int $tackles);

    public function getTackles(): ? int;

    public function setTouches(int $touches);

    public function getTouches(): ? int;

    public function setFouls(int $fouls);

    public function getFouls(): ? int;
}
