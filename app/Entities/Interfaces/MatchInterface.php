<?php

namespace App\Entities\Interfaces;

use DateTime;

/**
 * Abstract match, could be golf!
 */
interface MatchInterface {

    public function setCompetition(string $competition);

    public function getCompetition(): string;

    public function setMatchId(int $matchId);

    public function getMatchId(): int;

    public function setCreatedAt(string $createdAt);

    public function getCreatedAt(): DateTime;

    public function setUpdatedAt(string $updatedAt);

    public function getUpdatedAt(): DateTime;
}
