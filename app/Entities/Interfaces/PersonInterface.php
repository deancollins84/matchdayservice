<?php

namespace App\Entities\Interfaces;

/**
 * Common ground across players, managers, officials, etc ...
 */
interface PersonInterface {

    public function getFirstName() : string;

    public function getLastName() : string;
}
