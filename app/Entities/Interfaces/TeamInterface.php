<?php

namespace App\Entities\Interfaces;

use Illuminate\Support\Collection;

interface TeamInterface {

    public function setId(string $id);

    public function getId(): string;

    public function setName(string $name);

    public function getName(): string;

    public function getMatches(): Collection;

}
