<?php

namespace App\Entities\Interfaces;

//use App\Entities\Team;
use App\Entities\FootballMatchPlayerStat;

interface TeamPlayerInterface {

    //public function setTeam(Team $team);

    //public function getTeam(): ? Team;

    public function setKnownAs(string $knownAs);

    public function getKnownAs(): string;

    public function addFootballPlayerStats(FootballMatchPlayerStat $footballMatchPlayerStat);
}
