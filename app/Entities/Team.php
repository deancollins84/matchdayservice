<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Illuminate\Support\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Team implements Interfaces\TeamInterface {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=60)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $name;
    
    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="footballMatch", cascade={"persist"})
     */
    private $homeMatches;
    
    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="FootballMatch", mappedBy="footballMatch", cascade={"persist"})
     */
    private $awayMatches;

    public function __construct() {
        $this->homeMatches = new ArrayCollection();
        $this->awayMatches  = new ArrayCollection();
    }
    
    public function setId(string $id) {
        $this->id = $id;
    }

    public function getId(): string {
        return $this->id;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function getName(): string {
        return $this->name;
    }
    
    public function addHomeMatch($match){
        $this->homeMatches->add($homeMatch);
    }
    
    public function addAwayMatch($match){
        $this->awayMatches->add($homeMatch);
    }

    public function getMatches(): Collection {
        
    }

}
