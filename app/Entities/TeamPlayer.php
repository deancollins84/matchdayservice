<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class TeamPlayer extends AbstractPerson implements Interfaces\TeamPlayerInterface {
    
    /**
     * Decided to remove relationship with the player to the team in context of a match.
     * Many to one here does not necessary make sense i.e. 
     * player could be part of the domestic team, a national team or be on loan at another team.
     * Plus the added thought of players transferring from team to team and keeping the 
     * match data integrity in tact for future referencing. 
     * MOVED TO MATCH IT SELF for three way composite - make sense to me in context of a match!
     *  
     * Many players belong to one team.
     * ManyToOne(targetEntity="Team", inversedBy="players", cascade={"persist"})
     * JoinColumn(name="team_id", referencedColumnName="id")
     */
    //private $team;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $knownAs;

    /** 
     * One player can have many match stats.
     * @ORM\OneToMany(targetEntity="FootballMatchPlayerStat", mappedBy="teamPlayer", cascade={"persist"}) 
     */
    protected $footballMatchPlayerStats;
    
    public function __construct() {
        $this->footballMatchPlayerStats = new ArrayCollection();
    }
    
    public function setId(string $id){
        $this->id = $id;
    }
    
    public function getId() : string {
        return $this->id;
    }
    
    /*public function setTeam(Team $team){
        $this->team = $team;
    }*/
    
    /*public function getTeam(): ? Team {
        return $this->team;
    }*/

    public function setKnownAs(string $knownAs){
        $this->knownAs = $knownAs;
    }
    
    public function getKnownAs() : string {
        return $this->knownAs;
    }
    
    public function addFootballPlayerStats(FootballMatchPlayerStat $footballMatchPlayerStat){
        $this->footballMatchPlayerStat->add($footballMatchPlayerStat);
    }
    
}
