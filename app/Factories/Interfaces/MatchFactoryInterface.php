<?php

namespace App\Factories\Interfaces;

use App\Entities\AbstractMatch as Match;

interface MatchFactoryInterface {

    public function build(string $id = null, string $sport = null, string $competition = null, string $createdAt = null, string $updatedAt = null): ? Match;
    
}
