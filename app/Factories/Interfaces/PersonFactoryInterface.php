<?php

namespace App\Factories\Interfaces;

use App\Entities\AbstractPerson as Person;

interface PersonFactoryInterface {

    public function build(string $id = null, string $type = null, string $firstName = null, string $lastName = null): ? Person;
    
}
