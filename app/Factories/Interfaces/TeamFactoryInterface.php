<?php

namespace App\Factories\Interfaces;

use App\Entities\Team;

interface TeamFactoryInterface {

    public function build(string $id = null, string $name = null): ? Team;
    
}
