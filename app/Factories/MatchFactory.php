<?php

namespace App\Factories;

use App\Entities\AbstractMatch as Match;
use App\Entities\FootballMatch;

/**
 * For any other sport that gets added to the service, deal with creation here.
 */
class MatchFactory implements Interfaces\MatchFactoryInterface {

    public function build(string $id = null, string $sport = null, string $competition = null, string $createdAt = null, string $updatedAt = null): ? Match {
        if (($sport) && ($match = $this->getSport($sport)) && ($id)) {
            $match->setId($id);
            $match->setCompetition($competition); // In real world this would be an entity in own right.
            $match->setCreatedAt($createdAt ?? date("Y-m-d H:i:s"));
            $match->setUpdatedAt($createdAt ?? date("Y-m-d H:i:s"));
            return $match;
        }
        return null;
    }

    /**
     * Get the correct type of sport object.
     * @param string $sport
     * @return Match
     */
    protected function getSport(string $sport): ? Match {
        switch ($sport) {
            case 'football': $match = new FootballMatch; break;
            default: $match = null;
        }
        return $match;
    }

}