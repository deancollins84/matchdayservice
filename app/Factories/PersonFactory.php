<?php

namespace App\Factories;

use App\Entities\AbstractPerson as Person;
use App\Entities\TeamPlayer;

/**
 * For any other type of person that gets added to the service, deal with creation here.
 */
class PersonFactory implements Interfaces\PersonFactoryInterface {

    public function build(string $id = null, string $type = null, string $firstName = null, string $lastName = null): ? Person {
        if (($id) && ($person = $this->getPerson($type)) && ($firstName) && ($lastName)) {
            $person->setId($id);
            $person->setFirstName($firstName);
            $person->setLastName($lastName);
            return $person;
        }
        return null;
    }

    /**
     * Get the correct type of person object.
     * @param string $type
     * @return Person
     */
    protected function getPerson(string $type): ? Person {
        switch ($type) {
            case 'teamPlayer': $person = new TeamPlayer; break;
            // Managers 
            // Match officials
            // etc ...
            default: $person = null;
        }
        return $person;
    }

}