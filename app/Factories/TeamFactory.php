<?php

namespace App\Factories;

use App\Entities\Team;

class TeamFactory implements Interfaces\TeamFactoryInterface {

    public function build(string $id = null, string $name = null): ? Team {
        if (($id) && ($name)) {
            $team = new Team;
            $team->setId($id);
            $team->setName($name);
            return $team;
        }
        return null;
    }


}