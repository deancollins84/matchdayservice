<?php

namespace App\Http\Controllers\Interfaces;

use App\Http\Requests\MatchRequest;
use Illuminate\Http\JsonResponse;

interface MatchApiControllerInterface {

    public function postRequest(MatchRequest $request): JsonResponse;

    public function getResponse(string $sport, int $matchId): JsonResponse;
}
