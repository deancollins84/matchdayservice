<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\MatchApiControllerInterface;
use App\Http\Requests\MatchRequest;
use App\Repositories\Interfaces\MatchRepositoryInterface as MatchRepository;
use App\Services\FootballMatchService;
use App\Transformers\FootballMatchTransformer;
use Illuminate\Http\JsonResponse;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Resource\Item;
use App\Services\Interfaces\FeedServiceInterface as FeedService;
use SimpleXmlElement;
use ErrorException;

class MatchApiController extends Controller implements MatchApiControllerInterface {

    protected $fractal;
    protected $matchService;
    protected $feedService;
    protected $matchRepository;

    public function __construct(Manager $fractal, FootballMatchService $matchService, FeedService $feedService, MatchRepository $matchRepository) {
        $this->fractal = $fractal->setSerializer(new ArraySerializer);
        $this->matchService = $matchService;
        $this->feedService = $feedService;
        $this->matchRepository = $matchRepository;
    }

    public function postRequest(MatchRequest $request): JsonResponse {
        $requestCollection = collect($request->all())->recursive();
        $matchStats = $this->retrieveFeed($request->input('feed_file') ?? null);

        if ($match = $this->matchRepository->findById($request->get('match_id'))) {
           $match = $this->matchService->updateMatchData($match, $requestCollection->get('updated_at'), $matchStats);
        } else {
           $match = $this->matchService->addMatchData($requestCollection, $matchStats);
        }

        if(!$match->getFootballPlayerStats()->isEmpty()){
            return response()->json($this->fractal->createData(new Item($match, new FootballMatchTransformer))->toArray(), 202);
        } 
        throw new ErrorException('No stats found for this match', 404);
    }

    public function getResponse(string $sport, int $matchId): JsonResponse {
        if(in_array($sport, config('sport.types'))){
            if($matchId && $match = $this->matchRepository->findById($matchId)){
                if(!$match->getFootballPlayerStats()->isEmpty()){
                    return response()->json($this->fractal->createData(new Item($match, new FootballMatchTransformer))->toArray(), 200);
                } 
                throw new ErrorException('No stats found for this match', 404);
            }
            return response()->json(['error' => 'Not found'], 404);
        }
        return response()->json(['error' => 'Sport not supported'], 422);
    }
    
    /**
     * Go get the feed and return in as Simple XML
     * @param string $feedFile
     * @return SimpleXmlElement
     */
    protected function retrieveFeed(string $feedFile = null) : ? SimpleXmlElement {
        if(($feedFile) && ($xml = $this->feedService->retrieveFeed($feedFile))) {
          return simplexml_load_string($xml);
        }    
        return null;
    }

}
