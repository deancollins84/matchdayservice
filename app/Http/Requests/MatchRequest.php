<?php

namespace App\Http\Requests;

use AlbertCht\Form\FormRequest;

class MatchRequest extends FormRequest {

    public function rules() {
        return [
            'competition' => 'required|string',
            'match_id' => 'required|integer',
            'season' => 'required|integer',
            'sport' => 'required|string|sport',
            'teams' => 'required|array',
            'created_at' => 'required|date',
            'updated_at' => 'required|date',
            'feed_file' => 'required' // url maybe?
        ];
    }

}
