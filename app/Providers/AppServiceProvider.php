<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;
use Validator;
use GuzzleHttp\Client as GuzzleClient;
use App\Services\FeedService;
use App\Services\FileClient;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Validation.
        Validator::extend('sport', 'App\Validation\SportValidation@validateSport');
        
        Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                        if (is_array($value) || is_object($value)) {
                            return collect($value)->recursive();
                        }
                        return $value;
                    });
        });
        
        // Snapshot pattern for testing, to save time stubbing - to work in time frame.
        $this->app->singleton('App\Services\Interfaces\FeedServiceInterface', function () {
            if ((config('app.env') == 'local') || (config('app.env') == 'testing')) {
                return new FeedService(
                    new FileClient(['base_uri' => 'api.url'])
                );
            }
            return new FeedService(GuzzleClient);  
        });

    }

}