<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Interfaces as Interfaces;
use App\Repositories as Repositories;

class RepositoryServiceProvider extends ServiceProvider {

    public function register() {
        $this->app->bind(Interfaces\MatchRepositoryInterface::class, Repositories\DoctrineMatchRepository::class);  
        $this->app->bind(Interfaces\TeamRepositoryInterface::class, Repositories\DoctrineTeamRepository::class);  
        $this->app->bind(Interfaces\TeamPlayerRepositoryInterface::class, Repositories\DoctrineTeamPlayerRepository::class);  
    }

}
