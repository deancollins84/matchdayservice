<?php

namespace App\Repositories;

use Doctrine\Common\Persistence\ManagerRegistry;

abstract class DoctrineAbstractRepository  {

    protected $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }
    
    abstract function repository();

    public function findById($id = false) {
        if ($id) {
            return $this->repository()->find($id);
        }
        return null;
    }
}