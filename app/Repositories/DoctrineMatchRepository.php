<?php

namespace App\Repositories;

use App\Entities\AbstractMatch as Match;

class DoctrineMatchRepository extends DoctrineAbstractRepository implements Interfaces\MatchRepositoryInterface {

    public function repository() {
        return $this->doctrine->getRepository(Match::class);
    }



}
