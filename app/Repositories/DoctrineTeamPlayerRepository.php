<?php

namespace App\Repositories;

use App\Entities\TeamPlayer;

class DoctrineTeamPlayerRepository extends DoctrineAbstractRepository implements Interfaces\TeamPlayerRepositoryInterface {

    public function repository() {
        return $this->doctrine->getRepository(TeamPlayer::class);
    }

}