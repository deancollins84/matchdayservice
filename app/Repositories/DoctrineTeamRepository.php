<?php

namespace App\Repositories;

use App\Entities\Team;

class DoctrineTeamRepository extends DoctrineAbstractRepository implements Interfaces\TeamRepositoryInterface {

    public function repository() {
        return $this->doctrine->getRepository(Team::class);
    }



}
