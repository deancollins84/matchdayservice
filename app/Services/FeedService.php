<?php

namespace App\Services;

/**
 * To be completed further ...
 * for production, Guzzle Http Client would get switched in.
 */
class FeedService implements Interfaces\FeedServiceInterface {

    protected $client;
    
    /**
     * Ideal world with more time would wrap guzzle client to use own interfaces.
     * @param object $client
     */
    public function __construct(object $client) {
        $this->client = $client;
    }
    
    public function retrieveFeed(string $feed) { 
        return $this->client->get($feed);
        // Guzzle would return a repsonse that would need to be dealt with.
    }
    
}