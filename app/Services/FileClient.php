<?php

namespace App\Services;

class FileClient {

    public function get(string $path) {
        return file_get_contents($path, true);
    }

}
