<?php

namespace App\Services;

use Illuminate\Support\Collection;
use App\Entities\AbstractMatch as Match;
use App\Entities\FootballMatchPlayerStat;
use App\Factories\MatchFactory;
use App\Factories\TeamFactory;
use App\Factories\PersonFactory;
use App\Repositories\Interfaces\MatchRepositoryInterface as MatchRepository;
use App\Repositories\Interfaces\TeamRepositoryInterface as TeamRepository;
use App\Repositories\Interfaces\TeamPlayerRepositoryInterface as TeamPlayerRepository;
use Doctrine\ORM\EntityManager as DoctrineEntityManager;
use SimpleXMLElement;
use ErrorException;

class FootballMatchService implements Interfaces\FootballMatchServiceInterface {

    protected $doctrineEntityManager;
    protected $matchRepository;
    protected $teamRepository;
    protected $teamPlayerRepository;

    public function __construct(DoctrineEntityManager $doctrineEntityManager, MatchRepository $matchRepository, TeamRepository $teamRepository, TeamPlayerRepository $teamPlayerRepository) {
        $this->doctrineEntityManager = $doctrineEntityManager;
        $this->matchRepository = $matchRepository;
        $this->teamRepository = $teamRepository;
        $this->teamPlayerRepository = $teamPlayerRepository;
    }

    /**
     * Add new match and create the teams and players if needed.
     * @param Collection $data
     * @param SimpleXmlElement $matchStats
     * @throws ErrorException
     * @return Match
     */
    public function addMatchData(Collection $data, SimpleXmlElement $matchStats): ? Match {
        // Find or create match.
        $footballMatch = $this->matchRepository->findById($data->get('match_id'));
        if(!$footballMatch){
            $matchFactory = new MatchFactory;
            $footballMatch = $matchFactory->build($data->get('match_id'),  $data->get('sport'), $data->get('competition'), $data->get('created_at'), $data->get('updated_at'));
            $footballMatch->setSeason($data->get('season'));
        } else {
            throw new ErrorException('Match already exists', 409);
        }
              
        foreach ($matchStats->SoccerDocument->xpath('Team[@uID]') as $team) {
            
            $teamId = (string) $team->attributes()->uID;
            $teamData = $matchStats->xpath('SoccerDocument/MatchData[*]/TeamData[@TeamRef="' . $teamId . '"]')[0];
            $side = (string) $teamData->attributes()->Side;
    
            // Find or create team.
            $footballTeam = $this->teamRepository->findById($teamId);
            if(!$footballTeam){
                $teamFactory = new TeamFactory;
                $footballTeam = $teamFactory->build($teamId, $team->Name);
            }
            
            if($side == 'Home'){
                $footballMatch->setHomeTeam($footballTeam);
            } else {
                $footballMatch->setAwayTeam($footballTeam);
            }
               
            foreach ($team->Player as $player) {
                
                $playerId = (string) $player->attributes()->uID;

                // Find or create player.
                $teamPlayer = $this->teamPlayerRepository->findById($playerId);
                if(!$teamPlayer){
                    $teamPlayer = new PersonFactory;
                    $teamPlayer = $teamPlayer->build($playerId, 'teamPlayer', $player->PersonName->First, $player->PersonName->Last);
                    $teamPlayer->setKnownAs($player->PersonName->KnownAs ?? $player->PersonName->Last);
                }

                $stat = new FootballMatchPlayerStat;
                $stat->setFootballMatch($footballMatch);
                $stat->setSide(($side == 'Home') ? 1 : 0);
                $stat->setTeamPlayer($teamPlayer);
                $stat->setGoals($this->getStat($playerId, 'goals', $matchStats));
                $stat->setYellowCards($this->getStat($playerId, 'yellow_card', $matchStats));
                $stat->setRedCards($this->getStat($playerId, 'red_card', $matchStats));
                $stat->setTackles($this->getStat($playerId, 'total_tackle', $matchStats));
                $stat->setTouches($this->getStat($playerId, 'touches', $matchStats));
                $stat->setFouls($this->getStat($playerId, 'fouls', $matchStats));
                
                $footballMatch->addFootballPlayerStat($stat);
                
                $this->doctrineEntityManager->persist($stat);    
            } 
        }
        $this->doctrineEntityManager->flush();
        
        return $footballMatch;
    }
        
 
    /**
     * Update match stats.
     * @param Match $footballMatch
     * @param string $updatedAt
     * @param SimpleXMLElement $matchStats
     * @return Match
     */
    public function updateMatchData(Match $footballMatch, string $updatedAt, SimpleXMLElement $matchStats): Match { 
        foreach($footballMatch->getFootballPlayerStats() as $stat){   
            $playerId = $stat->getTeamPlayer()->getId();
            
            $stat->setGoals($this->getStat($playerId, 'goals', $matchStats));
            $stat->setYellowCards($this->getStat($playerId, 'yellow_card', $matchStats));
            $stat->setRedCards($this->getStat($playerId, 'red_card', $matchStats));
            $stat->setTackles($this->getStat($playerId, 'total_tackle', $matchStats));
            $stat->setTouches($this->getStat($playerId, 'touches', $matchStats));
            $stat->setFouls($this->getStat($playerId, 'fouls', $matchStats));
            $this->doctrineEntityManager->merge($stat);   
        }
        
        $footballMatch->setUpdatedAt($updatedAt);   
        $this->doctrineEntityManager->merge($footballMatch);
        $this->doctrineEntityManager->flush();

        return $footballMatch;
    }
    
    /**
     * Take a player ID and the stat to look up and return it.
     * @param string $playerId
     * @param string $statType
     * @return int
     */
    protected function getStat(string $playerId, string $statType, SimpleXMLElement $matchStats) : int {
        $stat = $matchStats->xpath('SoccerDocument/MatchData[*]/TeamData/PlayerLineUp/MatchPlayer[@PlayerRef="' . $playerId . '"]/Stat[@Type="' . $statType . '"]');

        if(isset($stat[0])){
            return (int) $stat[0];
        }

        return 0;
    }    

}
