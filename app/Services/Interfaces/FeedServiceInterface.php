<?php

namespace App\Services\Interfaces;

interface FeedServiceInterface {

    public function retrieveFeed(string $feed);
}
