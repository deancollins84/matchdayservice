<?php

namespace App\Services\Interfaces;

use Illuminate\Support\Collection;
use App\Entities\AbstractMatch as Match;
use SimpleXMLElement;

interface FootballMatchServiceInterface  {

    public function addMatchData(Collection $data, SimpleXmlElement $matchStats) : ? Match;
    
    public function updateMatchData(Match $match, string $updatedAt, SimpleXmlElement $matchStats) : Match;

}
