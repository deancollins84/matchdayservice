<?php

namespace App\Services\Interfaces;

use Illuminate\Support\Collection;

interface XmlParser {
    public function returnStats() : Collection;
}
