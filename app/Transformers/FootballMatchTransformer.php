<?php

namespace App\Transformers;

use App\Entities\FootballMatch;
use League\Fractal\TransformerAbstract;

class FootballMatchTransformer extends TransformerAbstract {
    
    /**
     * Transform match.
     * @param FootballMatch $match
     * @return array
     */
    public function transform(FootballMatch $match) : array {

       return [
            'competition' => (string) $match->getCompetition(),
            'match_id' => (int) $match->getMatchId(),
            'season' => (int) $match->getSeason(),
            'sport' => (string) $match->__toString(),
            'teams' => [
                'home' => (string) $match->getHomeTeam()->getName(),
                'away' => (string) $match->getAwayTeam()->getName()
            ],
            'created_at' => (string) $match->getCreatedAt()->format('Y-m-d H:i:s'),
            'updated_at' => (string) $match->getUpdatedAt()->format('Y-m-d H:i:s'),
            'stats' => [
                "top_scorer" => (string) $match->getTopScorer()->getKnownAs(),
                "winner" => (string) $match->getWinner()->getName(),
                "total_goals" => (int) $match->getTotalGoals(),
                "red_cards" => (int) $match->getTotalRedCards(),
                "yellow_cards" => (int) $match->getTotalYellowCards(),
                "home" => [
                  "total_tackles" => (int) $match->getHomeTeamTotalTackles(),
                  "total_touches" => (int) $match->getHomeTeamTotalTouches(),
                  "total_fouls" => (int) $match->getHomeTeamTotalFouls()
                ],
                "away" => [
                  "total_tackles" => (int) $match->getAwayTeamTotalTackles(),
                  "total_touches" => (int) $match->getAwayTeamTotalTouches(),
                  "total_fouls" => (int) $match->getAwayTeamTotalFouls()
                ]
            ]
        ];
    }

}
