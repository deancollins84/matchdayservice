<?php

namespace App\Validation;

class SportValidation {
    
    /**
     * Validate the sport.
     * @param string $attribute
     * @param string $value
     * @return boolean
     */
    public function validateSport($attribute, $value){
        $sports = config('sport.types');

        if (in_array($value, $sports)) {
                return true;
        }
        
        return false;
    }
}