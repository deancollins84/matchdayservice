<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20181125135702 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE matches (id VARCHAR(60) NOT NULL, competition VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, sport VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE football_matches (id VARCHAR(60) NOT NULL, home_team_id VARCHAR(60) DEFAULT NULL, away_team_id VARCHAR(60) DEFAULT NULL, season INT NOT NULL, INDEX IDX_32A020A9C4C13F6 (home_team_id), INDEX IDX_32A020A45185D02 (away_team_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE football_match_player_stats (football_match_id VARCHAR(60) NOT NULL, team_player_id VARCHAR(60) NOT NULL, side TINYINT(1) NOT NULL, goals INT NOT NULL, yellow_cards INT NOT NULL, red_cards INT NOT NULL, tackles INT NOT NULL, touches INT NOT NULL, fouls INT NOT NULL, INDEX IDX_E99F3405E1DA134D (football_match_id), INDEX IDX_E99F34052EE3EB38 (team_player_id), PRIMARY KEY(football_match_id, team_player_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE people (id VARCHAR(60) NOT NULL, first_name VARCHAR(150) NOT NULL, last_name VARCHAR(150) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_players (id VARCHAR(60) NOT NULL, known_as VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teams (id VARCHAR(60) NOT NULL, name VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE football_matches ADD CONSTRAINT FK_32A020A9C4C13F6 FOREIGN KEY (home_team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE football_matches ADD CONSTRAINT FK_32A020A45185D02 FOREIGN KEY (away_team_id) REFERENCES teams (id)');
        $this->addSql('ALTER TABLE football_matches ADD CONSTRAINT FK_32A020ABF396750 FOREIGN KEY (id) REFERENCES matches (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE football_match_player_stats ADD CONSTRAINT FK_E99F3405E1DA134D FOREIGN KEY (football_match_id) REFERENCES football_matches (id)');
        $this->addSql('ALTER TABLE football_match_player_stats ADD CONSTRAINT FK_E99F34052EE3EB38 FOREIGN KEY (team_player_id) REFERENCES team_players (id)');
        $this->addSql('ALTER TABLE team_players ADD CONSTRAINT FK_D9373291BF396750 FOREIGN KEY (id) REFERENCES people (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE football_matches DROP FOREIGN KEY FK_32A020ABF396750');
        $this->addSql('ALTER TABLE football_match_player_stats DROP FOREIGN KEY FK_E99F3405E1DA134D');
        $this->addSql('ALTER TABLE team_players DROP FOREIGN KEY FK_D9373291BF396750');
        $this->addSql('ALTER TABLE football_match_player_stats DROP FOREIGN KEY FK_E99F34052EE3EB38');
        $this->addSql('ALTER TABLE football_matches DROP FOREIGN KEY FK_32A020A9C4C13F6');
        $this->addSql('ALTER TABLE football_matches DROP FOREIGN KEY FK_32A020A45185D02');
        $this->addSql('DROP TABLE matches');
        $this->addSql('DROP TABLE football_matches');
        $this->addSql('DROP TABLE football_match_player_stats');
        $this->addSql('DROP TABLE people');
        $this->addSql('DROP TABLE team_players');
        $this->addSql('DROP TABLE teams');
    }
}
