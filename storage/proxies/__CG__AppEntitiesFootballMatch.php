<?php

namespace DoctrineProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class FootballMatch extends \App\Entities\FootballMatch implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entities\\FootballMatch' . "\0" . 'season', 'footballMatchPlayerStats', 'id'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entities\\FootballMatch' . "\0" . 'season', 'footballMatchPlayerStats', 'id'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (FootballMatch $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setId(string $id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setSeason(int $season)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSeason', [$season]);

        return parent::setSeason($season);
    }

    /**
     * {@inheritDoc}
     */
    public function getSeason(): int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSeason', []);

        return parent::getSeason();
    }

    /**
     * {@inheritDoc}
     */
    public function addFootballPlayerStat(\App\Entities\FootballMatchPlayerStat $footballMatchPlayerStat)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addFootballPlayerStat', [$footballMatchPlayerStat]);

        return parent::addFootballPlayerStat($footballMatchPlayerStat);
    }

    /**
     * {@inheritDoc}
     */
    public function setFootballPlayerStats(array $footballMatchPlayerStats)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFootballPlayerStats', [$footballMatchPlayerStats]);

        return parent::setFootballPlayerStats($footballMatchPlayerStats);
    }

    /**
     * {@inheritDoc}
     */
    public function getFootballPlayerStats()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFootballPlayerStats', []);

        return parent::getFootballPlayerStats();
    }

    /**
     * {@inheritDoc}
     */
    public function getTopScorer(): ?\App\Entities\TeamPlayer
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTopScorer', []);

        return parent::getTopScorer();
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalGoals(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotalGoals', []);

        return parent::getTotalGoals();
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalRedCards(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotalRedCards', []);

        return parent::getTotalRedCards();
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalYellowCards(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotalYellowCards', []);

        return parent::getTotalYellowCards();
    }

    /**
     * {@inheritDoc}
     */
    public function getHomeTeamTotalTackles(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHomeTeamTotalTackles', []);

        return parent::getHomeTeamTotalTackles();
    }

    /**
     * {@inheritDoc}
     */
    public function getHomeTeamTotalTouches(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHomeTeamTotalTouches', []);

        return parent::getHomeTeamTotalTouches();
    }

    /**
     * {@inheritDoc}
     */
    public function getHomeTeamTotalFouls(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHomeTeamTotalFouls', []);

        return parent::getHomeTeamTotalFouls();
    }

    /**
     * {@inheritDoc}
     */
    public function getAwayTeamTotalTackles(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAwayTeamTotalTackles', []);

        return parent::getAwayTeamTotalTackles();
    }

    /**
     * {@inheritDoc}
     */
    public function getAwayTeamTotalTouches(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAwayTeamTotalTouches', []);

        return parent::getAwayTeamTotalTouches();
    }

    /**
     * {@inheritDoc}
     */
    public function getAwayTeamTotalFouls(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAwayTeamTotalFouls', []);

        return parent::getAwayTeamTotalFouls();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompetition(string $competition)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompetition', [$competition]);

        return parent::setCompetition($competition);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompetition(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompetition', []);

        return parent::getCompetition();
    }

    /**
     * {@inheritDoc}
     */
    public function setMatchId(int $matchId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMatchId', [$matchId]);

        return parent::setMatchId($matchId);
    }

    /**
     * {@inheritDoc}
     */
    public function getMatchId(): int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMatchId', []);

        return parent::getMatchId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(string $createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): \DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(string $updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): \DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function getWinner(): ?object
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWinner', []);

        return parent::getWinner();
    }

}
