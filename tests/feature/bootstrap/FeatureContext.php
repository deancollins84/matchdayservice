<?php

namespace Tests\Bootstrap;

use Behat\Gherkin\Node\PyStringNode;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use TestCase;
use Exception;

/**
 * Defines application features from the specific contexts.
 */
class FeatureContext extends TestCase implements Context {
    
    protected $response = null;
    protected $payload = null;

    /**
     * Ran out of time to set up test db environment as Doctrine 2 with mariadb does not
     * play nice with in memory sqlite.
     */
    public function __construct()
    {
        putenv('DB_CONNECTION=sqlite');
        putenv('DB_DATABASE=:memory:');
        parent::setUp();
    }
    
    /** @BeforeScenario */
    public function before(BeforeScenarioScope $scope)
    {
        $this->artisan('doctrine:schema:create');
    }
    
    /**
     * @Given I :arg1 request :arg2
     */
    public function iRequest($arg1, $arg2)
    {
        $this->response = $this->json($arg1, $arg2, (json_decode($this->payload, true) ?? []))->response;
    }

    /**
     * @Then the status code will be :arg1
     */
    public function theStatusCodeWillBe($arg1)
    {
        if ($this->assertResponseStatus($arg1)) {
            throw new Exception('Wrong status');
        }
    }

    /**
     * @Then the response will be valid JSON
     */
    public function theResponseWillBeValidJson()
    {
        if (empty($this->response)) {
            throw new Exception('Did not get a response from the API');
        } else {
            if (!$this->seeJson()) {
                throw new Exception('Response was not JSON\n');
            }
        }
    }

    /**
     * @Given I have the payload:
     */
    public function iHaveThePayload(PyStringNode $string)
    {
        $this->payload = (string) $string;
    }
}
