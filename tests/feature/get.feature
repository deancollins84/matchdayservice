@get
Feature: Get match data from the live match data service

Scenario: Attempt to get match data from a match with no record
    Given I "GET" request "/api/match-data/football/1"
    Then the status code will be 404

Scenario: Attempt to get match data from a unsupported sport
    Given I "GET" request "/api/match-data/golf/2723"
    Then the status code will be 422

Scenario: Attempt to get match data from a match missing an id
    Given I "GET" request "/api/match-data/golf/"
    Then the status code will be 404