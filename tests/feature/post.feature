@post
Feature: Posting updates to the live match data service

Scenario: Successfully create new match record on service
    Given I have the payload:
    """
    {
      "competition": "English Barclays Premier League",
      "match_id": 2723,
      "season": 2017,
      "sport": "football",
      "teams": {
        "home": "Arsenal",
        "away": "Leicester City"
      },
      "created_at": "2018-01-08 15:24:20",
      "updated_at": "2018-01-08 15:24:22",
      "feed_file": "/var/www/match-data-service/tests/feeds/sports/football/match-2723/20170811_214207.xml"
    }
    """
    When I "POST" request "/api/match-data"
    Then the status code will be 202
    And the response will be valid JSON

Scenario: Match record exists on service, update match data
    Given I have the payload:
    """
    {
      "competition": "English Barclays Premier League",
      "match_id": 2723,
      "season": 2017,
      "sport": "football",
      "teams": {
        "home": "Arsenal",
        "away": "Leicester City"
      },
      "created_at": "2018-01-08 15:24:20",
      "updated_at": "2018-01-08 15:24:22",
      "feed_file": "/var/www/match-data-service/tests/feeds/sports/football/match-2723/20170811_214207.xml"
    }
    """
    When I "POST" request "/api/match-data"
    Then the status code will be 202
    And the response will be valid JSON

Scenario: Validation errors with missing request body fields
    Given I have the payload:
    """
    {
      "competition": "English Barclays Premier League",
      "match_id": 2723,
      "season": 2017,
      "teams": {
        "home": "Arsenal",
        "away": "Leicester City"
      },
      "created_at": "2018-01-08 15:24:20",
      "updated_at": "2018-01-08 15:24:22",
      "feed_file": "/var/www/match-data-service/tests/feeds/sports/football/match-2723/20170811_214207.xml"
    }
    """
    When I "POST" request "/api/match-data"
    Then the status code will be 422