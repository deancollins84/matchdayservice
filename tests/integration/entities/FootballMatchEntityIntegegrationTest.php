<?php

use App\Entities\FootballMatch;
use App\Entities\FootballMatchPlayerStat;
use App\Entities\TeamPlayer;

/**
 * Mock entities would be moved from here and pulled in.
 */
class FootballMatchEntityIntegrationTest extends TestCase {

    protected $mockMatch;

    public function setUp() {
        $this->mockMatch = new FootballMatch;
        $this->mockMatch->setId('m1');
        $this->mockMatch->setSeason(2018);
        parent::setUp();
    }

    public function testMatchTotalYellowCards() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setYellowCards(1);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setYellowCards(1);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setYellowCards(1);

        $this->mockMatch->addFootballPlayerStat($mockStat1);
        $this->mockMatch->addFootballPlayerStat($mockStat2);
        $this->mockMatch->addFootballPlayerStat($mockStat3);

        $this->assertEquals($this->mockMatch->getTotalYellowCards(), 3);
    }

    public function testMatchTotalRedCards() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setRedCards(1);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setRedCards(1);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);

        $this->assertEquals($this->mockMatch->getTotalRedCards(), 2);
    }

    public function testMatchTotalGoals() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setGoals(3);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setGoals(3);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);

        $this->assertEquals($this->mockMatch->getTotalGoals(), 6);
    }

    public function testHomeTeamTotalTackles() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(true);
        $mockStat1->setTackles(10);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(true);
        $mockStat2->setTackles(8);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(false);
        $mockStat3->setTackles(30);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalTackles(), 18);
    }

    public function testAwayTeamTotalTackles() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(false);
        $mockStat1->setTackles(10);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(false);
        $mockStat2->setTackles(8);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(true);
        $mockStat3->setTackles(30);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalTackles(), 30);
    }
    
    public function testHomeTeamTotalTouches() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(true);
        $mockStat1->setTouches(100);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(true);
        $mockStat2->setTouches(80);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(false);
        $mockStat3->setTouches(300);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalTouches(), 180);
    }

    public function testAwayTeamTotalTouches() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(false);
        $mockStat1->setTouches(100);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(false);
        $mockStat2->setTouches(80);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(true);
        $mockStat3->setTouches(300);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalTouches(), 300);
    }
    
    public function testHomeTeamTotalFouls() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(true);
        $mockStat1->setFouls(4);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(true);
        $mockStat2->setFouls(8);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(false);
        $mockStat3->setFouls(2);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalFouls(), 12);
    }
    
    public function testAwayTeamTotalFouls() {
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(false);
        $mockStat1->setFouls(4);

        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(false);
        $mockStat2->setFouls(8);

        $mockStat3 = new FootballMatchPlayerStat;
        $mockStat3->setSide(true);
        $mockStat3->setFouls(2);

        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        $this->mockMatch->addFootballPlayerstat($mockStat3);

        $this->assertEquals($this->mockMatch->getHomeTeamTotalFouls(), 2);
    }
    
    public function testTopGoalScorer(){
        
        $player1 = new TeamPlayer();
        $player1->setKnownAs('Kane');
        
        $player2 = new TeamPlayer();
        $player2->setKnownAs('Mata');
        
        $mockStat1 = new FootballMatchPlayerStat;
        $mockStat1->setSide(false);
        $mockStat1->setGoals(8);
        $mockStat1->setTeamPlayer($player1);
        
        $mockStat2 = new FootballMatchPlayerStat;
        $mockStat2->setSide(false);
        $mockStat2->setGoals(2);
        $mockStat2->setTeamPlayer($player2);
        
        $this->mockMatch->addFootballPlayerstat($mockStat1);
        $this->mockMatch->addFootballPlayerstat($mockStat2);
        
        $topGoalScorer = $this->mockMatch->getTopScorer();
        
        $this->assertInstanceOf(TeamPlayer::class, $topGoalScorer);
        $this->assertEquals($topGoalScorer->getKnownAs(), 'Kane');
    }


}
