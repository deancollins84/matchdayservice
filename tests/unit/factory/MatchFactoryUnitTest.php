<?php

use App\Factories\MatchFactory;
use App\Entities\Interfaces\MatchInterface as Match;

class MatchFactoryUnitTest extends TestCase {

    protected $factory;

    public function setup() {
        $this->factory = new MatchFactory;
    }

    public function testMatchReturned() {
        $match = $this->factory->build(2723, 'football', 'English Barclays Premier League');
        $this->assertInstanceOf(Match::class, $match);
    }

    public function testNullReturned() {
        $match = $this->factory->build(null, null, null);
        $this->assertNull($match);
    }

    public function testNullReturnedIfMissingRequirments() {
        $match = $this->factory->build(null, 'football', 'English Barclays Premier League');
        $this->assertNull($match);
    }

}
