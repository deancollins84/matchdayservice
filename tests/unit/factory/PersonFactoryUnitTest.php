<?php

use App\Factories\PersonFactory;
use App\Entities\Interfaces\PersonInterface as Person;

class PersonFactoryUnitTest extends TestCase {

    protected $factory;

    public function setup() {
        $this->factory = new PersonFactory;
    }

    public function testPersonReturned() {
        $person = $this->factory->build('p1', 'teamPlayer', 'Harry', 'Kane');
        $this->assertInstanceOf(Person::class, $person);
    }

    public function testNullReturned() {
        $person = $this->factory->build(null, null, null);
        $this->assertNull($person);
    }

    public function testNullReturnedIfMissingRequirments() {
        $person = $this->factory->build(null, 'teamPlayer', 'Harry');
        $this->assertNull($person);
    }

}
