<?php

use App\Factories\TeamFactory;
use App\Entities\Interfaces\TeamInterface as Team;

class TeamFactoryUnitTest extends TestCase {

    protected $factory;

    public function setup() {
        $this->factory = new TeamFactory;
    }

    public function testTeamReturned() {
        $team = $this->factory->build('t1', 'Tottenham');
        $this->assertInstanceOf(Team::class, $team);
    }

    public function testNullReturned() {
        $team = $this->factory->build(null, null, null);
        $this->assertNull($team);
    }

    public function testNullReturnedIfMissingRequirments() {
        $team = $this->factory->build(null, 'Tottenham');
        $this->assertNull($team);
    }

}
