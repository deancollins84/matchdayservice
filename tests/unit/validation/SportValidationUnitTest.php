<?php

class SportValidationUnitTest extends TestCase {
    
    // Store sports that the service can accept in a config?
    public function testValidSport(){
        $validator = $this->app['validator']->make(['sport' => 'football'], ['sport' => 'sport']);
        $this->assertTrue($validator->passes());    
    }
    
    public function testInvalidSport(){
        $validator = $this->app['validator']->make(['sport' => 'golf'], ['sport' => 'sport']);
        $this->assertFalse($validator->passes());
    }
}